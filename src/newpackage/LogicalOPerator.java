/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

/**
 *
 * @author Mahesh
 */
public class LogicalOPerator {

    public static void main(String[] args) {
        System.out.println("Logical AND OPERATOR");
        //Logical And ooperator
        boolean a = true && true;
        boolean b = true && false;
        boolean c = false && true;
        boolean d = false && false;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

        // logical Or operator 
        System.out.println("logical Or operator ");

        boolean p = true || true;
        boolean q = true || false;
        boolean r = false || true;
        boolean s = false || false;
        System.out.println(p);
        System.out.println(q);
        System.out.println(r);
        System.out.println(s);

        // unary operator 
        System.out.println(" unary operator ");
        boolean x = !true;
        boolean y = !false;
        System.out.println(x);
        System.out.println(y);

        //Increment And Decrement
        System.out.println("increment operator");
        int e = 10;
        System.out.println(e++); // post increment
        System.out.println(++e); // pre increment

        System.out.println(" Decrement operator ");
        int f = 40;
        System.out.println(f--); // post decrement 
        System.out.println(--f); // pre decrement 

        // combined or compound operator 
        System.out.println("combined or compound operator");
        int g = 15;
        g += 2;
        System.out.println(g);

        int h = 20;
        h -= 2;
        System.out.println(h);

        int i = 21;
        i *= 3;
        System.out.println(i);

        int j = 45;
        j /= 5;
        System.out.println(j);
        int k = 29;
        k %= 3;
        System.out.println(k);

    }

}


